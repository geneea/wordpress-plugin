<?php
namespace GENEEA;

define('GENEEA_REST_NAMESPACE','geneea/v1');

/**
 * Class Rest
 * @package GENEEA
 */
class Rest extends \WP_REST_Controller{

	function __construct(){
		add_action('rest_api_init',array($this,'register_routes'));
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes(){

		register_rest_route(GENEEA_REST_NAMESPACE,'get-tags',array(
			array(
				'methods' => \WP_REST_Server::CREATABLE,
				'callback' => array($this,'get_tags'),
				'permission_callback' => array($this,'user_can_edit_post'),
				'args' => [
					'post_id' => [
					        'required' => true,
					        'validate_callback' => function($param, $request, $key) {
						        return is_numeric( $param );
					        }
                    ]
				]
			)
		));

		register_rest_route(GENEEA_REST_NAMESPACE,'import-image',array(
			array(
				'methods' => \WP_REST_Server::CREATABLE,
				'callback' => array($this,'import_image'),
				'permission_callback' => array($this,'user_can_edit_post'),
				'args' => [
					'url' => [
					        'required' => true
                    ]
				]
			)
		));

		register_rest_route(GENEEA_REST_NAMESPACE,'get-images',array(
			array(
				'methods' => \WP_REST_Server::CREATABLE,
				'callback' => array($this,'get_images'),
				'permission_callback' => array($this,'user_can_edit_post'),
                'args' => [
					'tags' => ['required' => true]
				]
			)
		));

		register_rest_route(GENEEA_REST_NAMESPACE,'save-tags',array(
			array(
				'methods' => \WP_REST_Server::CREATABLE,
				'callback' => array($this,'save_tags'),
				'permission_callback' => array($this,'user_can_edit_post'),
				'args' => [
					'post_id' => ['required' => true]
				]
			)
		));
	}

	/**
	 * Get tags from Geneea API
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	function get_tags($request){
		$post_id = $request->get_param('post_id');
		$post = get_post($post_id);
		
		if (!$post || is_wp_error($post_id))
			return new \WP_Error('post-not-found',__('Post not found', GENEEA_TEXTDOMAIN), array('status' => 401));

		$args = [
			'id' => $post_id,
			'title' => wp_strip_all_tags($post->post_title),
			'body' => wp_strip_all_tags(apply_filters('the_content',$post->post_content)),
			'perex' => wp_strip_all_tags($post->post_excerpt)
		];


		$geneea_api = new Api_Geneea();
		$tags = $geneea_api->get_tags($args);
		if (is_wp_error($tags))
			return $tags;

        $gpost = new Post($post_id);
        $suggested_tags = array_merge($gpost->get_ignored_tags(),$gpost->get_geneea_tags());
		$tags_to_ignore = [];
        foreach ( $suggested_tags as $key => $tag ) {
			$tags_to_ignore[] = $key;
        }

		ob_start(); ?>
        <span class="button" id="add-all-tags">Add all</span>
		<?php foreach ( $tags->tags as $tag ) {
		    if (in_array($tag->id, $tags_to_ignore))
		        continue;
		    ?>
            <div class="tag" data-text="<?php echo $tag->text;?>" data-id="<?php echo $tag->id;?>" data-type="<?php echo $tag->type;?>"><span class="add button"><?php _e('Add',GENEEA_TEXTDOMAIN); ?></span> <span class="ignore button" data-text="<?php echo $tag->text;?>"><?php _e('Ignore',GENEEA_TEXTDOMAIN); ?></span> <?php echo $tag->text;?></div>
        <?php }
        $html = ob_get_clean();

		return new \WP_REST_Response(array('status' => 'success','html'=> $html),200);
	}

	/**
	 * Import an image to WP Media Library
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	function import_image($request){
		$url = $request->get_param('url');
		$post_id = 0;

		require_once(ABSPATH . 'wp-admin/includes/media.php');
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		// Try to get the image
		$image = media_sideload_image($url, $post_id);
		
		if (is_wp_error($image))
			return $image;

		return new \WP_REST_Response(array('status' => 'success',200));
	}

	/**
	 * Get Images
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	function get_images($request){
	    
		$geneea = new Api_Geneea();
	    $tags = json_decode($request->get_param('tags'));
	    $all_keywords = [];

	    // Get the keywords for each tag
	    foreach ( $tags as $id => $value ) {
                $keywords = $geneea->get_keywords($id);

                if (!is_wp_error($keywords) && isset($keywords->keywords))
                     $all_keywords[] = $keywords->keywords;
		}

	    // Get service ID if requested
		$service_id = $request->get_param('service');

		$image = new Images();
		$images = $image->get_images($all_keywords, $service_id);

		$strings = [];
		foreach ( $all_keywords as $keyword_group ) {
			foreach ( $keyword_group as $keyword ) {
				$strings[] = $keyword;
			}
		}


		if (empty($images))
			return new \WP_Error('no-images-found',__('No images found', GENEEA_TEXTDOMAIN), array('status' => 401,'keywords' => $all_keywords));

		ob_start(); ?>
        <h4><?php printf('Keywords used for the search: %s',implode(',',$strings));?></h4>
        <div class="images-previews">
		<?php
		foreach ( $images as $i ) {
		    ?>
			<div class="image-preview" data-full-image="<?php echo $i->get_url(); ?>">
				<img src="<?php echo $i->get_thumbnail(); ?>" alt="<?php echo $i->get_tags(); ?>">
                <div class="details">
                    <p class="description"><?php echo $i->get_description();?></p>
                    <a href="<?php echo $i->get_url();?>" target="_blank"><p class="service"><?php echo $i->get_service();?></p></a>

                </div>

			</div>
        <?php } ?>
        </div>
        <?php

		$html = ob_get_clean();

		return new \WP_REST_Response(array('status' => 'success','html' => $html,'images' => $images,'keywords' => $all_keywords),200);
	}

	/**
	 * Save tags
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	function save_tags($request) {
	    $tags = json_decode($request->get_param('tags'));
		$tags_cleaned = [];
		if ($tags) {
			foreach ( $tags as $key => $tag ) {
				/** @var $ class_name */
				$tags_cleaned[$key] = $tag;
			}
		}

		$ignored_tags = json_decode($request->get_param('ignored_tags'));
		$ignored_tags_cleaned = [];
		if ($ignored_tags) {
			foreach ( $ignored_tags as $key => $tag  ) {
				/** @var $ class_name */
				$ignored_tags_cleaned[$key] = $tag;
			}
		}

        $post_id = $request->get_param('post_id');

		$post = new Post($post_id);
		$post->save_geneea_tags($tags_cleaned);
		$post->save_ignored_tags($ignored_tags_cleaned);

		wp_set_post_tags( $post_id, implode(',',$tags_cleaned), true );

		return new \WP_REST_Response(array('status' => 'success','tags' => $tags_cleaned,'ignored_tags'=> $ignored_tags_cleaned),200);
    }


	/**
     * Check if user can edit posts
     */
	function user_can_edit_post() {
	    return current_user_can('edit_posts');
    }
}

new Rest();
