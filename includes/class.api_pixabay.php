<?php
namespace GENEEA;
use WPProgramator\Api;

/**
 * Class Api_Pixabay
 * @package GENEEA
 */
class Api_Pixabay extends Api
{
	private $access_key = '';
	private $api_url = 'https://pixabay.com/api/';

	/**
	 * Api_Pixabay constructor.
	 */
	function __construct()
	{
		$this->access_key = Init::$settings['pixabay_key'];

		$this->setApiUrl($this->api_url);
		$args = [
			'headers' => array(
				'User-agent' => sprintf('Geneea (%s)',site_url()),
				'Content-type' => 'application/json'
			)
		];

		$this->setArgs($args);
	}

	/**
	 * Get request URL
	 * @return string
	 */
	function getRequestUrl() {
		return $this->apiUrl . $this->endpoint;
	}

	/**
	 * Prepare the success response return
	 * To be overridden by sub-classes
	 * @return string
	 */
	function prepareResponseSuccess() {
		$body = json_decode($this->getResponseBody());
		return $body->hits;
	}

	/**
	 * Prepare the error response return
	 * To be overridden by sub-classes
	 * @return \WP_Error
	 */
	function prepareResponseError() {
		$errors = [];
		$body = json_decode($this->getResponseBody());

		foreach ($body->errors as $error) {
			$errors[] = $error->message;
		}

		return new \WP_Error(wp_remote_retrieve_response_code($this->response),implode(',',$errors), $body);
	}

	/**
	 * Search photos
	 * @param $args
	 * @return array|bool|mixed|\WP_Error
	 */
	function search_photos($args)
	{
		$query = utf8_uri_encode($args['q']);

		$this->setEndpoint("?key=$this->access_key&q=$query");
		return $this->_get();
	}

}