<?php
namespace GENEEA;

/**
 * Class Settings
 * @package GENEEA
 */
class Settings
{
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Start up
	 */
	public function __construct()
	{
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page()
	{
		// This page will be under "Settings"
		add_options_page(
			__('Geneea Settings',GENEEA_TEXTDOMAIN),
			__('Geneea Settings',GENEEA_TEXTDOMAIN),
			'manage_options',
			'geneea-settings',
			array( $this, 'create_admin_page' )
		);
	}

	/**
	 * Options page callback
	 */
	public function create_admin_page()
	{
		// Set class property
		$this->options = get_option( 'geneea_settings' );
		?>
		<div class="wrap">
			<h1><?php _e('Geneea Settings',GENEEA_TEXTDOMAIN); ?></h1>
			<form method="post" action="options.php">
				<?php
				// This prints out all hidden setting fields
				settings_fields( 'geneea_general_settings' );
				do_settings_sections( 'geneea-settings' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init()
	{
		register_setting(
			'geneea_general_settings', // Option group
			'geneea_settings', // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		add_settings_section(
			'general', // ID
			__('Geneea Settings',GENEEA_TEXTDOMAIN), // Title
			array( $this, 'print_section_info' ), // Callback
			'geneea-settings' // Page
		);

		add_settings_field(
			'api_key', // ID
			'API Key', // Title
			array( $this, 'api_key_callback' ), // Callback
			'geneea-settings', // Page
			'general' // Section
		);
		add_settings_field(
			'customer_id', // ID
			'Customer ID', // Title
			array( $this, 'customer_id_callback' ), // Callback
			'geneea-settings', // Page
			'general' // Section
		);
		add_settings_field(
			'pixabay_key', // ID
			'Pixabay API Key', // Title
			array( $this, 'pixabay_api_key_callback' ), // Callback
			'geneea-settings', // Page
			'general' // Section
		);

		add_settings_field(
			'shutterstock_key', // ID
			'Shutterstock API Key', // Title
			array( $this, 'shutterstock_api_key_callback' ), // Callback
			'geneea-settings', // Page
			'general' // Section
		);

		add_settings_field(
			'shutterstock_secret', // ID
			'Shutterstock API Secret', // Title
			array( $this, 'shutterstock_api_secret_callback' ), // Callback
			'geneea-settings', // Page
			'general' // Section
		);

		add_settings_field(
			'flickr_key', // ID
			'Flickr API Key', // Title
			array( $this, 'flickr_api_key_callback' ), // Callback
			'geneea-settings', // Page
			'general' // Section
		);

		add_settings_field(
			'flickr_secret', // ID
			'Flickr API Secret', // Title
			array( $this, 'flickr_api_secret_callback' ), // Callback
			'geneea-settings', // Page
			'general' // Section
		);


	}
	/**
	 * Sanitize each setting field as needed
	 * @param array $input Contains all settings fields as array keys
	 *
	 * @return array
	 */
	public function sanitize( $input )
	{
		$new_input = array();

		if( isset( $input['api_key'] ) )
			$new_input['api_key'] = sanitize_text_field( $input['api_key'] );

		if( isset( $input['customer_id'] ) )
			$new_input['customer_id'] = sanitize_text_field( $input['customer_id'] );

		if( isset( $input['pixabay_key'] ) )
			$new_input['pixabay_key'] = sanitize_text_field( $input['pixabay_key'] );

		if( isset( $input['shutterstock_key'] ) )
			$new_input['shutterstock_key'] = sanitize_text_field( $input['shutterstock_key'] );

		if( isset( $input['shutterstock_secret'] ) )
			$new_input['shutterstock_secret'] = sanitize_text_field( $input['shutterstock_secret'] );

		if( isset( $input['flickr_key'] ) )
			$new_input['flickr_key'] = sanitize_text_field( $input['flickr_key'] );

		if( isset( $input['flickr_secret'] ) )
			$new_input['flickr_secret'] = sanitize_text_field( $input['flickr_secret'] );

		return $new_input;
	}


	/**
	 * Get the settings option array and print one of its values
	 */
	public function api_key_callback()
	{
		printf(
			'<input type="text" id="api_key" name="geneea_settings[api_key]" value="%s" />',
			isset( $this->options['api_key'] ) ? esc_attr( $this->options['api_key']) : ''
		);
	}

	/**
	 * Get the settings option array and print one of its values
	 */
	public function customer_id_callback()
	{
		printf(
			'<input type="text" id="customer_id" name="geneea_settings[customer_id]" value="%s" />',
			isset( $this->options['customer_id'] ) ? esc_attr( $this->options['customer_id']) : ''
		);
	}
	/**
	 * Get the settings option array and print one of its values
	 */
	public function pixabay_api_key_callback()
	{
		printf(
			'<input type="text" id="pixabay_key" name="geneea_settings[pixabay_key]" value="%s" />',
			isset( $this->options['pixabay_key'] ) ? esc_attr( $this->options['pixabay_key']) : ''
		);
	}

	public function shutterstock_api_key_callback()
	{
		printf(
			'<input type="text" id="shutterstock_key" name="geneea_settings[shutterstock_key]" value="%s" />',
			isset( $this->options['shutterstock_key'] ) ? esc_attr( $this->options['shutterstock_key']) : ''
		);
	}

	public function shutterstock_api_secret_callback()
	{
		printf(
			'<input type="text" id="shutterstock_secret" name="geneea_settings[shutterstock_secret]" value="%s" />',
			isset( $this->options['shutterstock_secret'] ) ? esc_attr( $this->options['shutterstock_secret']) : ''
		);
	}

	public function flickr_api_key_callback()
	{
		printf(
			'<input type="text" id="flickr_key" name="geneea_settings[flickr_key]" value="%s" />',
			isset( $this->options['flickr_key'] ) ? esc_attr( $this->options['flickr_key']) : ''
		);
	}

	public function flickr_api_secret_callback()
	{
		printf(
			'<input type="text" id="flickr_secret" name="geneea_settings[flickr_secret]" value="%s" />',
			isset( $this->options['flickr_secret'] ) ? esc_attr( $this->options['flickr_secret']) : ''
		);
	}
	function print_section_info() {

    }


}

if( is_admin() )
	new Settings();