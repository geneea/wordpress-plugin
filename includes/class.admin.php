<?php

namespace GENEEA;

/**
 * Class Admin
 * @package GENEEA
 */
class Admin {
	function __construct() {
	}

	function initialize() {
		add_action( 'add_meta_boxes', array( $this, 'register_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_post_data' ) );

	}

	/**
	 * Register metaboxes
	 */
	function register_meta_boxes() {
		add_meta_box( 'geneea', __( 'Geneea', GENEEA_TEXTDOMAIN ), array( $this, 'render_metabox' ), [ 'post', 'page' ] );
	}

	/**
	 * Render the metabox
	 */
	function render_metabox() {
	    global $pagenow, $post;

	    $gpost = new Post($post->ID);
		if ('post-new.php' == $pagenow) { ?>
		<h3><?php _e('Save the post content first to get the Geneea suggestions',GENEEA_TEXTDOMAIN); ?></h3>
        <?php return;
		} ?>

        <div class="geneea">
            <div class="col">
<!--                <p>Add your own tags</p>-->
<!--                    <input type="text" id="geneea-extra-tags" name="geneea-extra-tags"/>-->
<!--                    <div id="add-extra-tag" class="button ld-ext-left">-->
<!--                        --><?php //_e( 'Add', GENEEA_TEXTDOMAIN ); ?>
<!--                        <div class="ld ld-ring ld-spin"></div>-->
<!--                    </div>-->
                <h3><?php _e('Selected tags',GENEEA_TEXTDOMAIN); ?></h3>
                <p><input type="text" name="geneea-post-tags" id="geneea-post-tags"/></p>
	            <?php
	            $string = json_encode($gpost->get_geneea_tags());
	            $str = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
		            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
	            }, $string);
	            ?>
                <p><input type="hidden" name="geneea-post-tags-object" id="geneea-post-tags-object" value='<?php echo $str;?>'/></p>

                <input type="hidden" name="geneea-delete-tags" id="geneea-delete-tags"/>
            </div>
            <div id="geneea-suggest" class="col">
                <div class="header">
                    <div id="suggest-tags" class="button ld-ext-left">
		                <?php _e( 'Suggest tags', GENEEA_TEXTDOMAIN ); ?>
                        <div class="ld ld-ring ld-spin"></div>
                    </div>


                </div>
                <div class="tags">

                </div>
                <h3><?php _e('Ignored tags',GENEEA_TEXTDOMAIN); ?></h3>
                <div id="ignored-tags">
	                <?php
	                $string = json_encode($gpost->get_ignored_tags());
	                $str = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
		                return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
	                }, $string);
	                ?>
                    <p><input type="text" name="geneea-ignored-tags" id="geneea-ignored-tags"/></p>
                    <p><input type="hidden" name="geneea-ignored-tags-object" id="geneea-ignored-tags-object" value='<?php echo $str;?>'/></p>
                </div>
            </div>

            <div id="geneea-find-images" class="button ld-ext-left">
		        <?php _e( 'Find images', GENEEA_TEXTDOMAIN ); ?>
                <div class="ld ld-ring ld-spin"></div>
            </div>
            <select name="geneea-image-service" id="geneea-image-service">
                <option value=""><?php _e('All services',GENEEA_TEXTDOMAIN); ?></option>
		        <?php
		        $images = new Images();
		        foreach ( $images->get_image_services() as $key => $service ) { ?>
                    <option value="<?php echo $key;?>"><?php echo $key;?></option>
		        <?php } ?>
            </select>

            <div class="images"></div>
        </div>
	<?php }

	/**
     * Save or delete tags for post
	 * @param $post_id
	 */
	function save_post_data( $post_id ) {
		// Delete post tags first if requested
		if ( isset( $_POST['geneea-delete-tags'] ) && ! empty( $_POST['geneea-delete-tags'] ) ) {
			$tags_to_remove = explode( ',', $_POST['geneea-delete-tags'] );
			foreach ( $tags_to_remove as $tag ) {
				$term = get_term_by( 'name', $tag, 'post_tag' );
				if ( $term && ! is_wp_error( $term ) ) {
					wp_remove_object_terms( $post_id, $term->term_id, 'post_tag' );
				}
			}
		}
	}
}

$Admin = new Admin();
$Admin->initialize();