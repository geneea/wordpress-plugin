<?php
namespace GENEEA;

/**
 * Class Image_Service
 * @package GENEEA
 */
abstract class Image_Service
{
	/**
	 * @param string $search
	 *
	 * @return Image[]
	 */
	abstract protected function get_images($search = '');
}