<?php
namespace GENEEA;
use WPProgramator\Api;

/**
 * Class Api_Pixabay
 * @package GENEEA
 */
class Api_Shutterstock extends Api
{
	private $key = '';
	private $secret = '';
	private $api_url = 'https://api.shutterstock.com/v2/';



	/**
	 * Api_Pixabay constructor.
	 */
	function __construct()
	{
		$this->key = Init::$settings['shutterstock_key'];
		$this->secret = Init::$settings['shutterstock_secret'];

		$this->setApiUrl($this->api_url);
		$args = [
			'headers' => array(
				'Authorization' => 'Basic ' . base64_encode($this->key.':'.$this->secret),
				'User-agent' => sprintf('Geneea (%s)',site_url()),
				'Content-type' => 'application/json'
			)
		];

		$this->setArgs($args);
	}

	/**
	 * Get request URL
	 * @return string
	 */
	function getRequestUrl() {
		return $this->apiUrl . $this->endpoint;
	}

	/**
	 * Prepare the success response return
	 * To be overridden by sub-classes
	 * @return string
	 */
	function prepareResponseSuccess() {
		$body = json_decode($this->getResponseBody());
		return $body->data;
	}

	/**
	 * Prepare the error response return
	 * To be overridden by sub-classes
	 * @return \WP_Error
	 */
	function prepareResponseError() {
		$errors = [];
		$body = json_decode($this->getResponseBody());
		
		foreach ($body->errors as $error) {
			$errors[] = $error->message;
		}

		return new \WP_Error(wp_remote_retrieve_response_code($this->response),implode(',',$errors), $body);
	}

	/**
	 * Search photos
	 * @param $args
	 * @return array|bool|mixed|\WP_Error
	 */
	function search_photos($args)
	{

		$keywords = [];
		foreach ( $args['q'] as $keywords_group ) {
			$strings = [];
			foreach ( $keywords_group as $keyword ) {
				$strings[] = "($keyword )";	
			}
			$keywords[] = implode(' OR ',$strings);
		}

		foreach ( $keywords as $key => $keyword ) {
			$keywords[$key] = "($keyword)";
		}

		$strings = implode(' AND ',$keywords);

		$this->setEndpoint("images/search?query=$strings");
		return $this->_get();
	}
}