<?php
namespace GENEEA;

/**
 * Class Post
 * @package GENEEA
 */
class Post {
	public $post_id;
    function __construct($post_id)
    {
    	$this->post_id = (int) $post_id;
    }

	/**
	 * Get Geneea post tags
	 * @return mixed
	 */
    function get_geneea_tags() {
	    $tags = get_post_meta($this->post_id,GENEEA_PREFIX.'tags',true);
	    if (empty($tags))
		    $tags = [];
	    return $tags;
    }

	/**
	 * Save the tags
	 * @param $tags
	 *
	 * @return bool|int
	 */
    function save_geneea_tags($tags) {
    	return update_post_meta($this->post_id,GENEEA_PREFIX.'tags', $tags);
    }

	/**
	 * Get ignored tags
	 * @return mixed
	 */
	function get_ignored_tags() {
		$tags = get_post_meta($this->post_id,GENEEA_PREFIX.'ignored_tags',true);
		if (empty($tags))
			$tags = [];
		return $tags;
	}

	/**
	 * Save ignored tags
	 * @param $tags
	 *
	 * @return bool|int
	 */
	function save_ignored_tags($tags) {
		return update_post_meta($this->post_id,GENEEA_PREFIX.'ignored_tags', $tags);
	}
}