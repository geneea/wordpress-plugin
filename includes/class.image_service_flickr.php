<?php
namespace GENEEA;

/**
 * Class Flickr
 * @package GENEEA
 */
class Flickr extends Image_Service {

	/**
	 * Get images from the service
	 * @param array $search
	 *
	 * @return Image[]
	 */
    function get_images( $search = [] ) {
		$api = new Api_Flickr();

		$args = [
		    'q' => $search
	    ];
	    $photos = $api->search_photos($args);

	    $images = [];
	    $counter = 0;
	    if (isset($photos->photos->photo)) {
		    foreach ( $photos->photos->photo as $photo ) {
			    if ($counter > 20)
				    break;

			    $details = $api->get_photo_sizes($photo->id);
			    if (isset($details->sizes)) {
				    $url = '';
				    $thumbnail_url = '';

				    foreach ( $details->sizes->size as $size ) {
					    if ($size->label == 'Medium')
						    $thumbnail_url = $size->source;

					    if ($size->label == 'Original')
						    $url = $size->source;

				    }

				    $image = new Image($url,$thumbnail_url,'Flickr', $photo->title);
				    $image->set_tags('');
				    $images[] = $image;
			    }
			    $counter++;
		    }
	    }


	    return $images;

    }
}

add_filter('geneea_image_services','\GENEEA\register_flickr');
function register_flickr($services) {
	$services['flickr'] = '\GENEEA\Flickr';
	return $services;
}