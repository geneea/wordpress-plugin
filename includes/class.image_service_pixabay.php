<?php
namespace GENEEA;

/**
 * Class Pixabay
 * @package GENEEA
 */
class Pixabay extends Image_Service {

	/**
	 * @param string $search
	 *
	 * @return array|Image[]
	 */
    function get_images( $search = '' ) {
		$api = new Api_Pixabay();

		$args = [
		    'q' => $search
	    ];
	    $photos = $api->search_photos($args);

	    $images = [];
	    foreach ( $photos as $photo ) {

	    	$image = new Image($photo->largeImageURL,$photo->webformatURL);
	    	$image->set_tags($photo->tags);
	    	$images[] = $image;
	    }

	    return $images;
    }
}

add_filter('geneea_image_services','\GENEEA\register_pixabay');
function register_pixabay($services) {
	$services['pixabay'] = '\GENEEA\Pixabay';
	return $services;
}