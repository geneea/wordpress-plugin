<?php
namespace GENEEA;

/**
 * Class Images
 * @package GENEEA
 */
class Images {
	public $images = [];

	/**
	 * Get the images
	 * @param string $search
	 *
	 * @return Image[]
	 */
    function get_images($search = '', $service_id = '') {
    	$images = [];
	    foreach ( $this->get_image_services() as $key => $service ) {
			if ($service_id && $key != $service_id)
				continue;

		    // Check if the clsss exists, bail if not
	    	if (!class_exists($service))
		    	continue;

	    	// The get_images method has to be implemented in the class
		    $s = new $service;
		    if (!method_exists($s,'get_images'))
		    	continue;

		    // Check if we got an array
		    $service_images = $s->get_images($search);
		    if (!is_array($service_images))
		    	continue;

			// Basic validation for image URL and thumbnail
		    $service_images = array_filter($service_images, function($v, $k) {
		    	return $v->get_url() && $v->get_thumbnail();
		    }, ARRAY_FILTER_USE_BOTH);

		    // Just merge the return
			$images = array_merge($images, $service_images);
	    }

    	$this->images = $images;
    	return apply_filters('geneea_images',$this->clean_images());
    }

	/**
	 * Basic images validation
	 * @return array
	 */
    function clean_images() {
	    foreach ( $this->images as $key => $image ) {
		    /** @var $image Image */
		    if (empty($image->get_url()))
		    	unset($this->images[$key]);
		}

		return $this->images;
    }

	/**
	 * Get image services
	 * This is the method that allows us easily to add a new image service just by implementing a class
	 * that extends Image_service class
	 * @return mixed|void
	 */
    function get_image_services() {
	    return apply_filters('geneea_image_services', []);
    }
}