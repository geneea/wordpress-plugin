<?php
namespace GENEEA;
use WPProgramator\Api;

/**
 * Class Api_Geneea
 * @package GENEEA
 */
class Api_Geneea extends Api
{
	private $api_key = '';
	private $customer_id = '';
	private $api_url = 'https://api.geneea.com/';


	protected $endpoint_tags = 'media/tags';
	protected $endpoint_items = 'media/items';


	function __construct()
	{
		$this->api_key = Init::$settings['api_key'];
		$this->customer_id = Init::$settings['customer_id'];

		$this->setApiUrl($this->api_url);
		$args = [
			'headers' => array(
				'User-agent' => sprintf('Geneea (%s)',site_url()),
				'Authorization' => 'user_key ' . $this->api_key,
				'Content-type' => 'application/json',
				'X-Customer-ID' => $this->customer_id
			)
		];

		$this->setArgs($args);
		$this->jsonEncodeBody = true;
	}

	/**
	 * Get request URL
	 * @return string
	 */
	function getRequestUrl() {
		return $this->apiUrl  . $this->endpoint;
	}

	/**
	 * Prepare the success response return
	 * To be overridden by sub-classes
	 * @return string
	 */
	function prepareResponseSuccess() {

		return json_decode($this->getResponseBody());
	}

	/**
	 * Prepare the error response return
	 * To be overridden by sub-classes
	 * @return \WP_Error
	 */
	function prepareResponseError() {

		$body = json_decode($this->getResponseBody());
		return new \WP_Error(wp_remote_retrieve_response_code($this->response),$body->message, $body);
	}

	/**
	 * Get suggested tags
	 * @param $args
	 * @return array|bool|mixed|object|\WP_Error
	 */
	function get_tags($args)
	{
		$args = [
			'body' => $args

		];

		$this->addArgs($args);

		$this->setEndpoint($this->endpoint_tags);
		return $this->_post();

	}

	function get_keywords($id) {
		$this->setEndpoint("$this->endpoint_items/$id/keywords?lang=en");
		return $this->_get();
	}



}


//add_action('template_redirect', '\SpreadCharts\test');
function test()
{
	$drip = new Drip();
	$args   = [
		'email'         => 'rreuocbv@10mail.org',
		'custom_fields' => [
			'country' => 'CZ',
			'report' => 'False',
		],
	];


	$result = $drip->add_subscriber_to_campaign(818902776, $args);
	//die(var_dump($result));

//    $result = WC()->session->set('customer',['email'=> 'dsadsa@ddasd.cz']);
//    die(var_dump($result));

//    $drip = new Drip();
//    $args = [
//        'email' => 'tereza.pitrincova@gs.cz',
//        'tag' => 'prepayment'
//    ];
//    $response = $drip->remove_tag($args);
//    die(var_dump($response));
}
