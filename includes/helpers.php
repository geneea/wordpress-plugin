<?php
/**
 * Get REST URL
 * @return string
 */
function geneea_get_rest_url() {
	return rest_url(GENEEA_REST_NAMESPACE);
}