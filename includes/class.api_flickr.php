<?php
namespace GENEEA;
use WPProgramator\Api;

/**
 * Class Api_Flickr
 * @package GENEEA
 */
class Api_Flickr extends Api
{
	private $key = '';
	private $secret = '';
	private $api_url = 'https://api.flickr.com/services/rest/';



	/**
	 * Api_Pixabay constructor.
	 */
	function __construct()
	{
		$this->key = Init::$settings['flickr_key'];
		$this->secret = Init::$settings['flickr_secret'];

		$this->setApiUrl($this->api_url);
		$args = [
			'headers' => array(
				'Authorization' => 'Basic ' . base64_encode($this->key.':'.$this->secret),
				'User-agent' => sprintf('Geneea (%s)',site_url()),
				'Content-type' => 'application/json'
			)
		];

		$this->setArgs($args);
	}

	/**
	 * Get request URL
	 * @return string
	 */
	function getRequestUrl() {
		return $this->apiUrl . $this->endpoint;
	}

	/**
	 * Prepare the success response return
	 * To be overridden by sub-classes
	 * @return string
	 */
	function prepareResponseSuccess() {
		$body = json_decode($this->getResponseBody());
		return $body;
	}

	/**
	 * Prepare the error response return
	 * To be overridden by sub-classes
	 * @return \WP_Error
	 */
	function prepareResponseError() {
		$errors = [];
		$body = json_decode($this->getResponseBody());
		
		foreach ($body->errors as $error) {
			$errors[] = $error->message;
		}

		return new \WP_Error(wp_remote_retrieve_response_code($this->response),implode(',',$errors), $body);
	}

	/**
	 * Search photos
	 * @param $args
	 * @return array|bool|mixed|\WP_Error
	 */
	function search_photos($args)
	{

		$keywords = [];
		foreach ( $args['q'] as $keywords_group ) {
			foreach ( $keywords_group as $keyword ) {
				$keywords[] = $keyword;
			}
		}

		$strings = implode(' OR ',$keywords);
		$this->setEndpoint("?method=flickr.photos.search&api_key=$this->key&text=$strings&format=json&nojsoncallback=1");
		return $this->_get();
	}

	function get_photo_sizes($photo_id) {
		$this->setEndpoint("?method=flickr.photos.getSizes&api_key=$this->key&photo_id=$photo_id&format=json&nojsoncallback=1");
		return $this->_get();
	}

}