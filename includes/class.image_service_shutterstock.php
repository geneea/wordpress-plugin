<?php
namespace GENEEA;

/**
 * Class Pixabay
 * @package GENEEA
 */
class Shutterstock extends Image_Service {

	/**
	 * @param string $search
	 *
	 * @return Image[]
	 */
    function get_images( $search = '' ) {
		$api = new Api_Shutterstock();

		$args = [
		    'q' => $search
	    ];
	    $photos = $api->search_photos($args);

	    $images = [];
	    if ($photos && !empty($photos) && !is_wp_error($photos)) {
		    foreach ( $photos as $photo ) {

			    $image = new Image($photo->assets->huge_thumb->url,$photo->assets->preview->url,'ShutterStock', $photo->description);
			    $image->set_tags('');
			    $images[] = $image;
		    }
	    }

	    return $images;
    }
}

add_filter('geneea_image_services','\GENEEA\register_shutterstock');
function register_shutterstock($services) {
	$services['shutterstock'] = '\GENEEA\Shutterstock';
	return $services;
}