<?php
namespace GENEEA;

/**
 * Class Image
 * @package GENEEA
 */
class Image {
    protected $url;
    protected $thumbnail;
    protected $description;
    protected $service;
    protected $tags;

    function __construct($url, $thumbnail_url, $service = '', $description = '') {
    	$this->set_url($url);
    	$this->set_thumbnail($thumbnail_url);
    	$this->set_service($service);
    	$this->set_description($description);
    }

	/**
	 * Set full image url
	 * @param $url
	 */
    function set_url($url) {
    	$this->url = esc_url_raw($url);
    }

	/**
	 * Get full image URL
	 * @return mixed
	 */
    function get_url() {
    	return $this->url;
    }

	/**
	 * Set image thumbnail
	 * @param $url
	 */
    function set_thumbnail($url) {
	    $this->thumbnail = esc_url_raw($url);
    }

	/**
	 * Get image thumbnail
	 * @return mixed
	 */
    function get_thumbnail() {
	    return $this->thumbnail;
    }

	/**
	 * Set image tags
	 * @param $tags
	 */
    function set_tags($tags) {
    	$this->tags = sanitize_text_field($tags);
    }

	/**
	 * Get image tags
	 * @return mixed
	 */
    function get_tags() {
    	return $this->tags;
    }

	/**
	 * Set service
	 * @param $service
	 */
	function set_service($service) {
		$this->service = sanitize_text_field($service);
	}

	/**
	 * Get image service
	 * @return mixed
	 */
	function get_service() {
		return $this->service;
	}

	/**
	 * Set description
	 * @param $description
	 */
	function set_description($description) {
		$this->description = sanitize_text_field($description);
	}

	/**
	 * Get image DESCRIPTION
	 * @return mixed
	 */
	function get_description() {
		return $this->description;
	}




}