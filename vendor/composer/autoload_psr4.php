<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'WPProgramator\\' => array($vendorDir . '/wpprogramator/wp-api-wrapper/src'),
);
