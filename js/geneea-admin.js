jQuery( document ).ready(function($) {
    var geneeaApp = {
        inputPostTags: $('input#geneea-post-tags'),
        inputPostTagsObject: $('input#geneea-post-tags-object'),
        inputIgnoredTags: $('input#geneea-ignored-tags'),
        inputIgnoredTagsObject: $('input#geneea-ignored-tags-object'),
        inputExtraPostTags: $('input#geneea-extra-tags'),
        buttonSuggestTags: $('#suggest-tags'),
        buttonFindImages: $('#geneea-find-images'),
        buttonAddTag: $('.geneea .tag .add'),
        body: $('body'),

        init: function() {
            this.tagEditor(this.inputPostTags,geneea.geneeaTags,this.inputPostTagsObject);
            this.tagEditor(this.inputIgnoredTags,geneea.geneeaIgnoredTags,this.inputIgnoredTagsObject);
            this.buttonSuggestTags.on('click',this.suggestTags);
            this.buttonFindImages.on('click',this.findImages);
            this.body.on('click', '.geneea .images .image-preview img', this.importImage);
            this.body.on('click','#add-all-tags',this.addAllTags);
            this.body.on('click','.geneea .tag .add', this.addTag);
            this.body.on('click','.geneea .tag .ignore',this.addIgnoreTag);
            },

        tagEditor: function(el,initialTags,object) {
            el.tagEditor({
                initialTags: initialTags,
                forceLowercase: false,

                beforeTagDelete: function(field, editor, tags, val) {
                    var objectField = geneeaApp.getObjectField(field);

                    var value = objectField.val();
                    var object = $.parseJSON(value);
                    for (var key in object) {
                        if (object[key] == val)
                        {
                            var tag = '<div class="tag" data-text="' + val + '" data-id="' + key + '"><span class="add button">Add</span> <span class="ignore button">Ignore</span> ' + val +' </div>';
                            $('.tags').append(tag);
                            delete object[key];
                        }
                    }

                    objectField.val(JSON.stringify(object));

                    $('#geneea-delete-tags').val(function(i,val) {
                        return val + (!val ? '' : ',') + value;
                    });
                    geneeaApp.saveTags();

                }
            });
        },

        addExtraTag: function() {
            $('#add-extra-tag').on('click',function() {
                console.log('aaa');
                $inputPostTags.tagEditor('addTag', $inputExtraPostTags.val());
                $inputExtraPostTags.val('');
            });

        },
        suggestTags: function() {
            var $this = $(this);
                geneeaApp.loader($this,'start');
                $.ajax({
                    url: geneea.restUrl + '/get-tags',
                    type: 'POST',
                    data: {
                        post_id: $('input[name="post_ID"]').val()
                    },
                    beforeSend: function ( xhr ) {
                        xhr.setRequestHeader( 'X-WP-Nonce', geneea.nonce );
                    },
                    success: function (response, statusText, xhr, $form) {
                        $('.tags').html(response.html);
                    },
                    error: function(xhr, textStatus) {
                        alert(xhr.responseJSON.message);
                    },
                    complete: function() {
                        geneeaApp.loader($this,'stop');
                    }
                });
        },
        importImage: function() {
            var r = confirm("Import to media library?");
            if (r == true) {
                $.ajax({
                    url: geneea.restUrl + '/import-image',
                    type: 'POST',
                    data: {
                        url: $(this).parent().data('full-image')
                    },
                    beforeSend: function ( xhr ) {
                        xhr.setRequestHeader( 'X-WP-Nonce', geneea.nonce );
                    },
                    success: function (response, statusText, xhr, $form) {
                        alert('Image imported to media library');
                    },
                    error: function(xhr, textStatus) {
                        alert(xhr.responseJSON.message);
                    }
                });
            }
        },
        findImages: function() {
            var $this = $(this);
            var tags = geneeaApp.inputPostTagsObject.val();
            geneeaApp.loader($this,'start');
            $.ajax({
                url: geneea.restUrl + '/get-images',
                type: 'POST',
                data: {
                    tags: tags,
                    service: $('#geneea-image-service').val()
                },
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', geneea.nonce );
                },
                success: function (response, statusText, xhr, $form) {
                    $('.geneea .images').html(response.html);
                },
                error: function(xhr, textStatus) {
                    alert(xhr.responseJSON.message);
                },
                complete: function() {
                    geneeaApp.loader($this,'stop');
                }
            });
        },
        addAllTags: function() {
            $('.geneea .tag .add').trigger('click');
        },
        addTag: function(e) {
            geneeaApp.inputPostTags.tagEditor('addTag', $(this).parent().data('text'));
            $(this).parent().remove();

            var key = $(this).parent().data('id');
            var value = $(this).parent().data('text');
            geneeaApp.setInputObjectValue(key,value,geneeaApp.inputPostTagsObject);
            geneeaApp.saveTags();

        },
        addIgnoreTag: function(e) {
            geneeaApp.inputIgnoredTags.tagEditor('addTag', $(this).parent().data('text'));
            $(this).parent().remove();

            var key = $(this).parent().data('id');
            var value = $(this).parent().data('text');
            geneeaApp.setInputObjectValue(key,value,geneeaApp.inputIgnoredTagsObject);
            geneeaApp.saveTags();

        },
        setInputObjectValue: function (key, value, object) {
            var data = {};
            data[key] = value;

            var val = object.val();
            if (!val || val == '[]' || val == {}) {
                object.val(JSON.stringify(data));
            } else {
                data = $.parseJSON(val);
                data[key] = value;
                object.val(JSON.stringify(data));
            }
        },
        getObjectField: function(field) {
            var object = false;
            switch(field.attr('id')) {
                case 'geneea-post-tags':
                    object = geneeaApp.inputPostTagsObject;
                    break;
                case 'geneea-ignored-tags':
                    object = geneeaApp.inputIgnoredTagsObject;
                    break;
            }

            return object;
        },
        loader: function (obj,action) {
            if (action == 'start') {
                obj.addClass('running');
            }
            if (action == 'stop') {
                obj.removeClass('running');
            }
        },
        saveTags: function () {
            $.ajax({
                url: geneea.restUrl + '/save-tags',
                type: 'POST',
                data: {
                    post_id: $('input[name="post_ID"]').val(),
                    tags: geneeaApp.inputPostTagsObject.val(),
                    ignored_tags: geneeaApp.inputIgnoredTagsObject.val()
                },
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', geneea.nonce );
                },
                success: function (response, statusText, xhr, $form) {
                    $('.tags').html(response.html);
                },
                error: function(xhr, textStatus) {
                    alert(xhr.responseJSON.message);
                }
            });
        }
    };

    geneeaApp.init();

});