=== Geneea ===
Contributors: vasikgreif, geneea
Tags: geneea, images, tags, automatic tags, photos, nlp
Requires at least: 4.6
Tested up to: 5.0
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Wordpress and Geneea integration