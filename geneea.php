<?php
/*
Plugin Name: Geneea
Plugin URI: https://geneea.com
Description: Wordpress and Geneea Tagger integration
Author: V�clav Greif, Geneea
Version: 1.0
Author URI: https://wp-programator.cz, https://geneea.com
Requires at least: 4.6
*/
namespace GENEEA;

DEFINE('GENEEA_PLUGIN_URL',plugins_url('/',__FILE__));
DEFINE('GENEEA_TEXTDOMAIN','geneea');
DEFINE('GENEEA_PREFIX','_geneea_');

final class Init
{
	public static $settings; 
    /**
     * Call this method to get singleton
     *
     * @return Init
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new Init();
        }
        return $inst;
    }

    /**
     * Private ctor so nobody else can instance it
     *
     */
    private function __construct()
    {
        $this->require_classes();
        $this->add_actions();
        self::$settings = get_option('geneea_settings',[]);

    }

    /**
     * Require classes
     */
    function require_classes() {
    	require_once('vendor/autoload.php');
    	require_once('includes/class.settings.php');
    	require_once('includes/class.admin.php');
    	require_once('includes/class.rest.php');
    	require_once('includes/class.api_geneea.php');

    	require_once('includes/class.images.php');
    	require_once('includes/class.image.php');
    	require_once('includes/class.image_service.php');

	    require_once('includes/class.api_shutterstock.php');
	    require_once('includes/class.image_service_shutterstock.php');
	    require_once('includes/class.api_flickr.php');
	    require_once('includes/class.image_service_flickr.php');


	    //require_once('includes/class.api_pixabay.php');
	    //require_once('includes/class.pixabay.php');

    	require_once('includes/class.post.php');
	    require_once('includes/helpers.php');
    }

    /**
     * Add actions
     */
    function add_actions() {
    	add_action( 'admin_enqueue_scripts', array($this,'enqueue_admin_scripts'));
    }

    /**
	 * Enqueue admin scripts
	 */
	function enqueue_admin_scripts() {
		wp_enqueue_script( 'caret', GENEEA_PLUGIN_URL . 'lib/jQuery-tagEditor/jquery.caret.min.js', array('jquery'), false, true );
		wp_enqueue_script( 'tag-editor', GENEEA_PLUGIN_URL . 'lib/jQuery-tagEditor/jquery.tag-editor.min.js', array('jquery'), false, true );
		wp_enqueue_style( 'tag-editor', GENEEA_PLUGIN_URL . 'lib/jQuery-tagEditor/jquery.tag-editor.css');
		wp_enqueue_script( 'geneea', GENEEA_PLUGIN_URL . 'js/geneea-admin.js', array('tag-editor'), false, true );
		wp_enqueue_style( 'loading-btn', GENEEA_PLUGIN_URL . 'lib/loading-btn/loading.css');
		wp_enqueue_style( 'loading-btn-css', GENEEA_PLUGIN_URL . 'lib/loading-btn/loading-btn.css');

		wp_enqueue_style( 'geneea', GENEEA_PLUGIN_URL . 'css/geneea-admin.css');
		$data = [
			'restUrl' => geneea_get_rest_url(),
			'nonce' => wp_create_nonce('wp_rest'),
			'geneeaTags' => []

		];
		global $post;
		if ($post) {
			$genea_post = new Post($post->ID);
			$data['geneeaTags'] = [];
			foreach ( $genea_post->get_geneea_tags() as $tag ) {
				$data['geneeaTags'][] = $tag;
			}

			$data['geneeaIgnoredTags'] = [];
			foreach ( $genea_post->get_ignored_tags() as $tag ) {
				$data['geneeaIgnoredTags'][] = $tag;

			}
		}

		wp_localize_script('geneea','geneea',$data);
	}
}

Init::Instance();